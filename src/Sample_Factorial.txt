        =========================================
               File:  Samples/factorial.elf
        =========================================
        =========================================
                        ELF Header
        =========================================

File-type:              Executable file (Type 2)
File class:             32-bit ELF File
Arch-type:              Advanced RISC Machine (ARM)
Byte encoding:          Two's complement, little endian
Entry point:            Memory Address 0x0
Prog-Header:            340 bytes into file
Prog-Header-count:      1
Section-Header:         372 bytes into file
Section-Header-count:   5


        =========================================
                Section Headers (5 values)
        =========================================


Index   Name                    Type            Memory Address*   Offset (Bytes)  Size (Bytes)
0       (NULL)                  Undefined                         0                 0
1       factorial.elf           Program Data    0x0               52                95
2       symtab                  Symbol Table                      148               112
3       strtab                  String Table                      260               40
4       shstrtab                String Table                      300               40

* Only applicable to program headers


        =========================================
                 Symbol Table (7 values)
        =========================================


Index   Name                    Address/Value                   Size        Type        Bind        Section Header Index
0       (NULL)                  Memory 0x0                      0           No Type     Local       Undefined
1       stack                   EQU    #4096                    0           No Type     Local       Compiler Constant
2       input                   EQU    #6                       0           No Type     Local       Compiler Constant
3       factorial               Memory 0x4                      0           No Type     Local       Program Header 1
4       exit                    Memory 0x20                     0           No Type     Local       Program Header 1
5       main                    Memory 0x28                     0           No Type     Local       Program Header 1
6       result                  Memory 0x50                     0           No Type     Local       Program Header 1


        =========================================
                     Program Header 1
        =========================================

Segment-Offset:         52 bytes
Size of segment:        95 bytes
Align:                  1
Memory Address:         0x0

Type:                   1
Flags:                  0

Instructions
============

Memory          Binary          Label                   Instruction                                                     DisARM Comments
00000000:       ea000008                                B       main                                                    main is stored at address 0x28
00000004:       e3510000        factorial               CMP     R1,     #0
00000008:       03a01001                                MOVEQ   R1,     #1
0000000c:       0a000003                                BEQ     exit                                                    exit is stored at address 0x20
00000010:       e92d4002                                STMFD   SP!,    {R1, LR}
00000014:       e2411001                                SUB     R1,     R1,     #1
00000018:       ebfffff9                                BL      factorial                                               factorial is stored at address 0x4
0000001c:       e8bd4002                                LDMFD   SP!,    {R1, LR}
00000020:       e0000190        exit                    MUL     R0,     R0,     R1
00000024:       e1a0f00e                                MOV     PC,     LR
00000028:       e3a01006        main                    MOV     R1,     #6
0000002c:       e3a0da01                                MOV     SP,     #4096                                           4096 = 0x1000
00000030:       e1a00001                                MOV     R0,     R1
00000034:       ef000004                                SWI     4
00000038:       e28f0010                                ADR     R0,     result                                          ADD R0, PC, #16 = #0x10
                                                                                                                        result is stored at address 0x50
0000003c:       ef000003                                SWI     3
00000040:       e3a00001                                MOV     R0,     #1
00000044:       ebffffee                                BL      factorial                                               factorial is stored at address 0x4
00000048:       ef000004                                SWI     4
0000004c:       ef000002                                SWI     2
00000050:       63616620        result                  CMNVS   R1,     #33554432                                       33554432 = 0x2000000
00000054:       69726f74                                LDMVSEA R2!,    {R2, R4-R6, R8-FP, SP-LR}
00000058:       69206c61                                STMVSFD R0!,    {R0, R5-R6, R10-FP, SP-LR}
0000005c:       00002073                                ANDEQ   R2,     R0,     R3, RRX

