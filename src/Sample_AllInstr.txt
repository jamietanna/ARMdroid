        =========================================
            File:  Samples/allinstructions.elf
        =========================================
        =========================================
                        ELF Header
        =========================================

File-type:              Executable file (Type 2)
File class:             32-bit ELF File
Arch-type:              Advanced RISC Machine (ARM)
Byte encoding:          Two's complement, little endian
Entry point:            Memory Address 0x4000
Prog-Header:            476 bytes into file
Prog-Header-count:      3
Section-Header:         572 bytes into file
Section-Header-count:   7


        =========================================
                Section Headers (7 values)
        =========================================


Index   Name                    Type            Memory Address*   Offset (Bytes)  Size (Bytes)
0       (NULL)                  Undefined                         0                 0
1       Samples/...ions.elf     Program Data    0x0               52                213
2       Samples/...ions.elf     Program Data    0x2000            265               28
3       Samples/...ions.elf     Program Data    0x4000            293               24
4       symtab                  Symbol Table                      320               80
5       strtab                  String Table                      400               24
6       shstrtab                String Table                      424               52

* Only applicable to program headers


        =========================================
                 Symbol Table (5 values)
        =========================================


Index   Name                    Address/Value                   Size        Type        Bind        Section Header Index
0       (NULL)                  Memory 0x0                      0           No Type     Local       Undefined
1       main                    Memory 0x0                      0           No Type     Local       Program Header 1
2       aval                    Memory 0xc0                     0           No Type     Local       Program Header 1
3       bval                    Memory 0xc4                     0           No Type     Local       Program Header 1
4       cval                    Memory 0x2000                   0           No Type     Local       Program Header 2


        =========================================
                     Program Header 1
        =========================================

Segment-Offset:         52 bytes
Size of segment:        213 bytes
Align:                  1
Memory Address:         0x0

Type:                   1
Flags:                  0

Instructions
============

Memory          Binary          Label                   Instruction                                                     DisARM Comments
00000000:       02810078        main                    ADDEQ   R0,     R1,     #120                                    120 = 0x5a  =  'Z'
00000004:       12412ffa                                SUBNE   R2,     R1,     #1000                                   1000 = 0x3e8
00000008:       a0621002                                RSBGE   R1,     R2,     R2
0000000c:       c0011002                                ANDGT   R1,     R1,     R2
00000010:       b1843005                                ORRLT   R3,     R4,     R5
00000014:       d0254006                                EORLE   R4,     R5,     R6
00000018:       81c65007                                BICHI   R5,     R6,     R7
0000001c:       93a06801                                MOVLS   R6,     #65536                                          65536 = 0x10000
00000020:       e1e0700e                                MVN     R7,     LR
00000024:       4358002a                                CMPMI   R8,     #42                                             42 = 0x5a  =  'Z'
00000028:       53790080                                CMNPL   R9,     #128
0000002c:       611a000b                                TSTVS   R10,    FP
00000030:       71340005                                TEQVC   R4,     R5
00000034:       e0000591                                MUL     R0,     R0,     R5
00000038:       e0313790                                MLAS    R1,     R3,     R7,     R0
0000003c:       e59f007c                                LDR     R0,     aval                                            aval is stored at address 0xc0
00000040:       e3a01ffa                                MOV     R1,     #1000                                           1000 = 0x3e8
00000044:       e7902181                                LDR     R2,     [R0, R1, LSL #3]
00000048:       e5d0000a                                LDRB    R0,     [R0, #10]
0000004c:       e4d0000a                                LDRB    R0,     [R0], #10
00000050:       e4885001                                STR     R5,     [R8], #1
00000054:       e7e07001                                STRB    R7,     [R0, R1]!
00000058:       58bdb6db                                LDMPLFD SP!,    {R0-R1, R3-R4, R6-R7, R9-R10, R12-SP, PC}
0000005c:       08bdc123                                LDMEQFD SP!,    {R0-R1, R5, R8, LR-PC}
00000060:       e810a002                                LDMFA   R0,     {R1, SP, PC}
00000064:       19bdc123                                LDMNEED SP!,    {R0-R1, R5, R8, LR-PC}
00000068:       5910a002                                LDMPLEA R0,     {R1, SP, PC}
0000006c:       e92dc123                                STMFD   SP!,    {R0-R1, R5, R8, LR-PC}
00000070:       e980a002                                STMFA   R0,     {R1, SP, PC}
00000074:       e82dc123                                STMED   SP!,    {R0-R1, R5, R8, LR-PC}
00000078:       e880a002                                STMEA   R0,     {R1, SP, PC}
0000007c:       ebffffdf                                BL      main                                                    main is stored at address 0x0
00000080:       ef000000                                SWI     0
00000084:       ef000001                                SWI     1
00000088:       ef000002                                SWI     2
0000008c:       ef000003                                SWI     3
00000090:       ef000004                                SWI     4
00000094:       e3e06000                                MVN     R6,     #0
00000098:       e28f0024                                ADR     R0,     bval                                            ADD R0, PC, #36 = #0x24
                                                                                                                        bval is stored at address 0xc4
0000009c:       e28f0fd7                                ADR     R0,                                                     ADD R0, PC, #860 = #0x35c
                                                                                                                        No label at 0x4219, therefore could be embedded in data, or it could be an ADRL
000000a0:       e2800b07                                ADD     R0,     R0,     #7168                                   7168 = 0x1c00
000000a4:       e1a00000                                NOP                                                             Pseudo-operator; MOV R0, R0
000000a8:       e0810185                                ADD     R0,     R1,     R5, LSL #3
000000ac:       e0801836                                ADD     R1,     R0,     R6, LSR R8
000000b0:       e06101e4                                RSB     R0,     R1,     R4, ROR #3
000000b4:       e0610064                                RSB     R0,     R1,     R4, RRX
000000b8:       e0424f51                                SUB     R4,     R2,     R1, ASR PC
000000bc:       eaffffcf                                B       main                                                    main is stored at address 0x0
000000c0:       000007d0        aval                    MULEQ   R0,     R0,     R7
000000c4:       73696854        bval                    CMNVC   R9,     #5505024                                        5505024 = 0x540000
000000c8:       20736920                                RSBCS-HSSR6,    R3,     R0, LSR #18                             18 = 0x12
000000cc:       74732061                                LDRVCB  R2,     [R3], #97!
000000d0:       676e6972                                STRVSB  R6,     [LR, R2, ROR #18]!
000000d4:       00000000                                ANDEQ   R0,     R0,     R0

        =========================================
                     Program Header 2
        =========================================

Segment-Offset:         265 bytes
Size of segment:        28 bytes
Align:                  1
Memory Address:         0x2000

Type:                   1
Flags:                  0

Instructions
============

Memory          Binary          Label                   Instruction                                                     DisARM Comments
00002000:       73696854        cval                    CMNVC   R9,     #5505024                                        5505024 = 0x540000
00002004:       72747320                                RSBVCS  R7,     R4,     #2147483648 = #0x80000000
00002008:       20676e69                                RSBCS-HSR6,     R7,     R9, ROR #28                             28 = 0x1c
0000200c:       61207369                                TEQVS   R0,     R9, ROR #6
00002010:       20736567                                RSBCS-HSSR6,    R3,     R7, ROR #10                             10 = 0x5a  =  '\n'
00002014:       79617761                                STMVCFD R1!,    {R0, R5-R6, R8-R10, R12-LR}
00002018:       004f3a20                                ADR     R3,                                                     SUB R3, PC, R0, LSR #20
                                                                                                                        No label at 0x5632, therefore could be embedded in data, or it could be an ADRL

        =========================================
                     Program Header 3
        =========================================

Segment-Offset:         293 bytes
Size of segment:        24 bytes
Align:                  1
Memory Address:         0x4000

Type:                   1
Flags:                  0

Instructions
============

Memory          Binary          Label                   Instruction                                                     DisARM Comments
00004000:       e3a0000a                                MOV     R0,     #10                                             10 = 0x5a  =  '\n'
00004004:       ef000000                                SWI     0
00004008:       e24f0010                                ADR     R0,                                                     SUB R0, PC, #16 = #0x10
                                                                                                                        No label at 0x16384, therefore could be embedded in data, or it could be an ADRL
0000400c:       e2400a02                                SUB     R0,     R0,     #8192                                   8192 = 0x2000
00004010:       ef000003                                SWI     3
00004014:       ef000002                                SWI     2

