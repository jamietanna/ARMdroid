package couk.jamietanna.disarm;

import couk.jamietanna.disarm.elf.ELFFile;

public class disARM {

	public static void main(String[] args) throws Exception {
		ELFFile e;

		e = ELFFile.decodeElfFile("ElfFiles/allinstructions.elf");
		e.output();

		System.out.println("\n\n");

		e = ELFFile.decodeElfFile("ElfFiles/factorial.elf");
		e.output();
	}

}
