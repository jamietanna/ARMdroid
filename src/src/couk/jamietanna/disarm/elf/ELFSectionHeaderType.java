package couk.jamietanna.disarm.elf;

public enum ELFSectionHeaderType {
	DYNSYM, NULL, PROGBITS, STRTAB, SYMTAB;
	@Override
	public String toString() {
		switch (this) {
		case NULL:
			return "Undefined";
		case PROGBITS:
			return "Program Data";
		case STRTAB:
			return "String Table";
		case SYMTAB:
			return "Symbol Table";
		case DYNSYM:
			return "Dynamic Symbol Table";
		default:
			return "--INVALID--";
		}
	};
}
