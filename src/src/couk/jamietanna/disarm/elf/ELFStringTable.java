package couk.jamietanna.disarm.elf;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Map.Entry;

public class ELFStringTable {

	private int numStrings = 0;

	private HashMap<Integer, String> theStrings = new HashMap<Integer, String>();

	public ELFStringTable(RandomAccessFile in, boolean isLittleEndian,
			ELFSectionHeaderEntry stringTable) throws IOException {
		// super(in, isLittleEndian);
		populate(in, isLittleEndian, stringTable);
	}

	public String getStringFrom(long name) {
		return theStrings.get((int) name);
	}

	public int getTableCount() {
		return numStrings;
	}

	public void output() {
		for (Entry<Integer, String> e : theStrings.entrySet())
			System.out.println(e.getKey() + "\t" + e.getValue());

		// for (String val : theStrings.values())
		// System.out.println(val);
	}

	// TODO ElfStringTable.populate() this needs a lot of refactoring!
	public void populate(RandomAccessFile in, boolean isLittleEndian,
			ELFSectionHeaderEntry table) throws IOException {
		// System.out.println("----");
		in.seek(table.getOffset());

		// System.out.println(table.size);

		// List<ElfStringTableEntry> strings = new
		// ArrayList<ElfStringTableEntry>();

		byte[] chars = new byte[(int) table.getSize()];
		char[] curr = new char[(int) table.getSize()];
		in.read(chars, 0, (int) table.getSize());

		int cIdx = 0;
		int firstIdx = 0;
		for (int i = 0; i < chars.length; i++) {
			byte ch = chars[i];
			if (ch == 0) {
				if (curr[0] != 0) {

					char[] temp = new char[cIdx];
					for (int j = 0; j < cIdx; j++)
						temp[j] = curr[j];

					// System.out.println(new String(temp) + " :: " + (i + 1));

					// strings.add(new ElfStringTableEntry(new String(temp), i +
					// 1));
					theStrings.put(firstIdx, new String(temp));
					curr = new char[(int) table.getSize() - i];
					cIdx = 0;
					firstIdx = 0;
					numStrings++;
				}
			} else {
				if (firstIdx == 0)
					firstIdx = i;
				curr[cIdx++] = (char) ch;
			}
		}

		// this.strings = new ElfStringTableEntry[strings.size()];
		// for (int i = 0; i < this.strings.length; i++) {
		// theStrings.put(i, arg1)
		// // this.strings[i] = strings.get(i);
		// // System.out.println(this.strings[i].getStartPos() + " + "
		// // + this.strings[i].getString());
		// }

	}
}
