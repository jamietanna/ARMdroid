package couk.jamietanna.disarm.elf;

import java.io.EOFException;
import java.io.IOException;
import java.io.RandomAccessFile;

import couk.jamietanna.helper.FileMethods;

public class ELFHeader {

	public static final byte[] ELF_MAGIC = { (byte) 127, (byte) 'E',
			(byte) 'L', (byte) 'F' };

	/*
	 * magic elfClass headerVersion padding type machineType version phdrOffset
	 * shdrOffset eHdrsize pHdrEntSize
	 */

	private long architecture; // unsigned short archtype;
	private long entry; // unsigned int entry;
	// public byte[] padding = new byte[9]; // unsigned char pad[9];
	private long fileType; // unsigned short filetype;
	private long flags; // unsigned int flags;
	private int hdrSize; // unsigned short hdrsize;
	private short headerVersion; // unsigned char hversion;
	private boolean is32Bit; // unsigned char elfClass;
	private boolean isLittleEndian; // unsigned char byteorder;
	private short[] magic = new short[4]; // unsigned char magic[4];
	private int pHdrCount; // unsigned short phdrcnt;
	private int pHdrEntSize; // unsigned short phdrent;
	private long phdrOffset; // unsigned int phdrpos;
	private int sHdrCount; // unsigned short shdrent;
	private int sHdrEntSize; // unsigned short shdrcnt;
	private long shdrOffset; // unsigned int shdrpos;
	private int sHdrStrIdx; // unsigned short strsec;
	private long version; // unsigned int fversion;

	public ELFHeader(RandomAccessFile in) {
		populate(in);
	}

	public long getArchitecture() {
		return architecture;
	}

	public long getEntry() {
		return entry;
	}

	public long getFileType() {
		return fileType;
	}

	public long getFlags() {
		return flags;
	}

	public int getHdrSize() {
		return hdrSize;
	}

	public short getHeaderVersion() {
		return headerVersion;
	}

	public short[] getMagic() {
		return magic;
	}

	public int getpHdrCount() {
		return pHdrCount;
	}

	public int getpHdrEntSize() {
		return pHdrEntSize;
	}

	public long getPhdrOffset() {
		return phdrOffset;
	}

	public int getsHdrCount() {
		return sHdrCount;
	}

	public int getsHdrEntSize() {
		return sHdrEntSize;
	}

	public long getShdrOffset() {
		return shdrOffset;
	}

	public int getsHdrStrIdx() {
		return sHdrStrIdx;
	}

	public long getVersion() {
		return version;
	}

	public boolean isIs32Bit() {
		return is32Bit;
	}

	public boolean isLittleEndian() {
		return isLittleEndian;
	}

	public boolean isValidHeader() {
		for (int i = 0; i < 4; i++)
			if (getMagic()[i] != ELF_MAGIC[i])
				return false;
		return true;
	}

	public void output() {

		/**
		 * eHdrsize = in.readShort(); pHdrEntSize = in.readShort(); pHdrCount =
		 * in.readShort(); sHdrEntSize = in.readShort(); sHdrCount =
		 * in.readShort(); sHdrStrIdx = in.readShort();
		 */

		System.out.println("Elf Class: " + (isIs32Bit() ? "32" : "64")
				+ " bit ELF File");
		System.out.println("Byte Order: "
				+ (isLittleEndian() ? "Little Endian" : "Big Endian"));
		System.out.println("Header Version: " + getHeaderVersion());
		System.out.println("File Type: " + getFileType());
		System.out.println("Machine Type: "
				+ (getArchitecture() == 40 ? "Advanced RISC Machine (ARM)"
						: getArchitecture()));
		System.out.println("#### Version: " + getVersion());
		System.out.println("#### Entry Point: " + getEntry());

		System.out.println("Flags: " + getFlags());

		System.out.println("!!!eHdrsize: " + getHdrSize());
		System.out.println("!!!pHdrEntSize: " + getpHdrEntSize());

		System.out.println("Program Header Offset: " + getPhdrOffset());
		System.out.println("Program Header Count: " + getpHdrCount());
		System.out.println("Section Header Offset: " + getShdrOffset());
		System.out.println("Section Header Count: " + getsHdrCount());
		// System.out.println("!!!strsec: " + );

	}

	// TODO http://jessicarbrown.com/resources/unsignedtojava.html
	public void populate(RandomAccessFile in) {
		try {
			for (int i = 0; i < 4; i++)
				getMagic()[i] = FileMethods.readUByte(in);

			if (FileMethods.readUByte(in) == 1)
				setIs32Bit(true);
			else
				setIs32Bit(false);

			if (FileMethods.readUByte(in) == 1)
				setLittleEndian(true);
			else
				setLittleEndian(false);

			setHeaderVersion(FileMethods.readUByte(in));

			// padding - 9 bytes
			for (int i = 0; i < 9; i++)
				FileMethods.readUByte(in);

			setFileType(FileMethods.readUShort(in));
			// switch(fileType){
			// this.fileType = ELFFileTypes.CORE_FILE;
			// }
			// this.fileType = ELFFileTypes.values()[fileType];

			setArchitecture(FileMethods.readUShort(in));

			setVersion(readUInt(in));
			setEntry(readUInt(in));
			setPhdrOffset(readUInt(in));
			setShdrOffset(readUInt(in));
			setFlags(readUInt(in));

			setHdrSize(FileMethods.readUShort(in));
			setpHdrEntSize(FileMethods.readUShort(in));
			setpHdrCount(FileMethods.readUShort(in));
			setsHdrEntSize(FileMethods.readUShort(in));
			setsHdrCount(FileMethods.readUShort(in));
			setsHdrStrIdx(FileMethods.readUShort(in));
		} catch (EOFException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public long readUInt(RandomAccessFile in) throws IOException {
		return FileMethods.readUInt(in, isLittleEndian());
	}

	public void setArchitecture(long architecture) {
		this.architecture = architecture;
	}

	public void setEntry(long entry) {
		this.entry = entry;
	}

	public void setFileType(long fileType) {
		this.fileType = fileType;
	}

	public void setFlags(long flags) {
		this.flags = flags;
	}

	public void setHdrSize(int hdrSize) {
		this.hdrSize = hdrSize;
	}

	public void setHeaderVersion(short headerVersion) {
		this.headerVersion = headerVersion;
	}

	public void setIs32Bit(boolean is32Bit) {
		this.is32Bit = is32Bit;
	}

	public void setLittleEndian(boolean isLittleEndian) {
		this.isLittleEndian = isLittleEndian;
	}

	public void setMagic(short[] magic) {
		this.magic = magic;
	}

	public void setpHdrCount(int pHdrCount) {
		this.pHdrCount = pHdrCount;
	}

	public void setpHdrEntSize(int pHdrEntSize) {
		this.pHdrEntSize = pHdrEntSize;
	}

	public void setPhdrOffset(long phdrOffset) {
		this.phdrOffset = phdrOffset;
	}

	public void setsHdrCount(int sHdrCount) {
		this.sHdrCount = sHdrCount;
	}

	public void setsHdrEntSize(int sHdrEntSize) {
		this.sHdrEntSize = sHdrEntSize;
	}

	public void setShdrOffset(long shdrOffset) {
		this.shdrOffset = shdrOffset;
	}

	public void setsHdrStrIdx(int sHdrStrIdx) {
		this.sHdrStrIdx = sHdrStrIdx;
	}

	public void setVersion(long version) {
		this.version = version;
	}
}
