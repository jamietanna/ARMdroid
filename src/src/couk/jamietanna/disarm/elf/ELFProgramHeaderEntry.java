package couk.jamietanna.disarm.elf;

import java.io.IOException;
import java.io.RandomAccessFile;

import couk.jamietanna.disarm.arm.ARMInstruction;
import couk.jamietanna.disarm.arm.IARMInstruction;
import couk.jamietanna.helper.FileMethods;

public class ELFProgramHeaderEntry {

	private long align;

	private long filesize;

	private long flags;

	private RandomAccessFile in;

	private IARMInstruction[] instructions;

	private boolean isLittleEndian;

	private long memsize;

	private long offset;

	private long physaddr;

	private long type;

	private long virtaddr;

	public ELFProgramHeaderEntry(RandomAccessFile in, boolean isLittleEndian)
			throws IOException {
		this.in = in;
		this.isLittleEndian = isLittleEndian;
	}

	public long getAlign() {
		return align;
	}

	public int getCount() {
		return (int) (((getFilesize() + 3) & ~3) / 4);
	}

	public long getFilesize() {
		return filesize;
	}

	public long getFlags() {
		return flags;
	}

	/**
	 * Getter
	 * 
	 * @return the instructions
	 */
	public IARMInstruction[] getInstructions() {
		return instructions;
	}

	public long getMemsize() {
		return memsize;
	}

	public long getOffset() {
		return offset;
	}

	public long getPhysaddr() {
		return physaddr;
	}

	public long getType() {
		return type;
	}

	public long getVirtaddr() {
		return virtaddr;
	}

	public void output() {
		System.out.println("Type\tOffset\tFlags\tAlign\tVirt\tPhys\tFsize\tMsize");
		System.out.print("\t");
		System.out.print(getType());
		System.out.print("\t");

		System.out.print(getOffset());
		System.out.print("\t");

		System.out.print(getFlags());
		System.out.print("\t");

		System.out.print(getAlign());
		System.out.print("\t");

		System.out.print(getVirtaddr());
		System.out.print("\t");

		System.out.print(getPhysaddr());
		System.out.print("\t");

		System.out.print(getFilesize());
		System.out.print("\t");

		System.out.print(getMemsize());
		System.out.print("\t");

		System.out.println();

		for (int i = 0; i < instructions.length; i++)
			System.out.println(String.format("0x%08x: %s", instructions[i].getMemoryAddress(),
					instructions[i]/* .getOperand() */.decode()));

		// for (ARMInstruction instr : instructions)
		// System.out.println(instr.decode());

	}

	public void populateInstructions() throws IOException {
		instructions = new IARMInstruction[getCount()];

		in.seek(offset);
		for (int i = 0; i < getCount(); i++) {
			long instruction = FileMethods.readUInt(in, isLittleEndian);
			instructions[i] = ARMInstruction.decodeInstruction(instruction, (int) (getPhysaddr() + (i) * 4));
		}
	}

	public void setAlign(long align) {
		this.align = align;
	}

	public void setFilesize(long filesize) {
		this.filesize = filesize;
	}

	public void setFlags(long flags) {
		this.flags = flags;
	}

	/**
	 * Setter
	 * 
	 * @param instructions
	 *            the instructions to set
	 */
	public void setInstructions(IARMInstruction[] instructions) {
		this.instructions = instructions;
	}

	public void setMemsize(long memsize) {
		this.memsize = memsize;
	}

	public void setOffset(long offset) {
		this.offset = offset;
	}

	public void setPhysaddr(long physaddr) {
		this.physaddr = physaddr;
	}

	public void setType(long type) {
		this.type = type;
	}

	public void setVirtaddr(long virtaddr) {
		this.virtaddr = virtaddr;
	}

}
