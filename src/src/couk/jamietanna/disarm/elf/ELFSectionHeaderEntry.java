package couk.jamietanna.disarm.elf;

import java.io.IOException;
import java.io.RandomAccessFile;

import couk.jamietanna.helper.FileMethods;

public class ELFSectionHeaderEntry {

	private static final int SHT_DYNAMIC = 6;
	private static final int SHT_DYNSYM = 11;
	private static final int SHT_HASH = 5;
	private static final int SHT_NOBITS = 8;
	private static final int SHT_NOTE = 7;
	private static final int SHT_NULL = 0;
	private static final int SHT_PROGBITS = 1;
	private static final int SHT_REL = 9;
	private static final int SHT_RELA = 4;
	private static final int SHT_SHLIB = 10;
	private static final int SHT_STRTAB = 3;
	private static final int SHT_SYMTAB = 2;

	private long addralign;
	private long address;
	private long entsize;
	private long flags;
	private long info;
	private long link;
	private long nameLong;
	private long offset;
	private long size;
	private ELFSectionHeaderType type;

	public ELFSectionHeaderEntry(RandomAccessFile in, boolean isLittleEndian)
			throws IOException {
		populate(in, isLittleEndian);
	}

	public long getAddralign() {
		return addralign;
	}

	public long getAddress() {
		return address;
	}

	public long getEntsize() {
		return entsize;
	}

	public long getFlags() {
		return flags;
	}

	public long getInfo() {
		return info;
	}

	public long getLink() {
		return link;
	}

	// TODO store the String, not the long - make it quicker/less lookups
	public String getName() {
		String n = ELFFile.getInstance().getStringFromSectHdr(getNameLong());
		if (n == null)
			return Integer.toString((int) getNameLong());
		else
			return n;
	}

	private long getNameLong() {
		return nameLong;
	}

	public long getOffset() {
		return offset;
	}

	public long getSize() {
		return size;
	}

	public ELFSectionHeaderType getType() {
		return type;
	}

	public void output() {
		System.out.print(getType() + "\t");
		System.out.print(getOffset() + "\t");
		System.out.print(getFlags() + "\t");
		System.out.print(getAddralign() + "\t");

		System.out.print(getName() + "\t");

		if (getType() == ELFSectionHeaderType.PROGBITS)
			System.out.print(String.format("0x%04x\t", getAddress()));
		else
			System.out.print("\t");

		System.out.print(getSize() + "\t");
		System.out.print(getLink() + "\t");
		System.out.print(getInfo() + "\t");
		System.out.print(getEntsize() + "\t");
		System.out.println();
	}

	public void populate(RandomAccessFile in, boolean isLittleEndian)
			throws IOException {
		setNameLong(FileMethods.readUInt(in, isLittleEndian));
		long type = FileMethods.readUInt(in, isLittleEndian);
		if (type == SHT_NULL)
			setType(ELFSectionHeaderType.NULL);
		else if (type == SHT_PROGBITS)
			setType(ELFSectionHeaderType.PROGBITS);
		else if (type == SHT_SYMTAB)
			setType(ELFSectionHeaderType.SYMTAB);
		else if (type == SHT_STRTAB)
			setType(ELFSectionHeaderType.STRTAB);
		else if (type == SHT_DYNSYM)
			setType(ELFSectionHeaderType.DYNSYM);

		setFlags(FileMethods.readUInt(in, isLittleEndian));
		setAddress(FileMethods.readUInt(in, isLittleEndian));
		setOffset(FileMethods.readUInt(in, isLittleEndian));
		setSize(FileMethods.readUInt(in, isLittleEndian));
		setLink(FileMethods.readUInt(in, isLittleEndian));
		setInfo(FileMethods.readUInt(in, isLittleEndian));
		setAddralign(FileMethods.readUInt(in, isLittleEndian));
		setEntsize(FileMethods.readUInt(in, isLittleEndian));

	}

	public void setAddralign(long addralign) {
		this.addralign = addralign;
	}

	public void setAddress(long address) {
		this.address = address;
	}

	public void setEntsize(long entsize) {
		this.entsize = entsize;
	}

	public void setFlags(long flags) {
		this.flags = flags;
	}

	public void setInfo(long info) {
		this.info = info;
	}

	public void setLink(long link) {
		this.link = link;
	}

	private void setNameLong(long nameLong) {
		this.nameLong = nameLong;
	}

	public void setOffset(long offset) {
		this.offset = offset;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public void setType(ELFSectionHeaderType type) {
		this.type = type;
	}

}
