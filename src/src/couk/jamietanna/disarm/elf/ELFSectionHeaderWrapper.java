package couk.jamietanna.disarm.elf;

import java.io.IOException;
import java.io.RandomAccessFile;

public class ELFSectionHeaderWrapper {

	private ELFHeader header;
	private ELFSectionHeaderEntry[] sectionHeaders;
	private ELFSectionHeaderEntry shstringTable;
	private ELFSectionHeaderEntry stringTable;
	private ELFSectionHeaderEntry symbolTable;

	public ELFSectionHeaderWrapper(RandomAccessFile in, ELFHeader header)
			throws IOException {
		this.header = header;
		populate(in);

	}

	public ELFSectionHeaderEntry getShstringTable() {
		return shstringTable;
	}

	public ELFSectionHeaderEntry getStringTable() {
		return stringTable;
	}

	public ELFSectionHeaderEntry getSymbolTable() {
		return symbolTable;
	}

	public void output() {
		System.out
				.println("Index\tType\t\tOffset\tFlags\tAlign\tName\tAddr\tSize\tLink\tInfo\tEntSize");
		for (int s = 0; s < sectionHeaders.length; s++) {
			System.out.print(s + "\t");
			sectionHeaders[s].output();
		}
	}

	public void populate(RandomAccessFile in) throws IOException {
		in.seek(header.getShdrOffset());

		sectionHeaders = new ELFSectionHeaderEntry[header.getsHdrCount()];

		for (int s = 0; s < header.getsHdrCount(); s++) {
			sectionHeaders[s] = new ELFSectionHeaderEntry(in,
					header.isLittleEndian());
			if (sectionHeaders[s].getType() != null)
				if (sectionHeaders[s].getType().equals(
						ELFSectionHeaderType.STRTAB))
					if (getStringTable() == null)
						setStringTable(sectionHeaders[s]);
					else
						setShstringTable(sectionHeaders[s]);
				else if (sectionHeaders[s].getType().equals(
						ELFSectionHeaderType.SYMTAB))
					setSymbolTable(sectionHeaders[s]);
		}
	}

	public void setShstringTable(ELFSectionHeaderEntry shstringTable) {
		this.shstringTable = shstringTable;
	}

	public void setStringTable(ELFSectionHeaderEntry stringTable) {
		this.stringTable = stringTable;
	}

	public void setSymbolTable(ELFSectionHeaderEntry symbolTable) {
		this.symbolTable = symbolTable;
	}

}
