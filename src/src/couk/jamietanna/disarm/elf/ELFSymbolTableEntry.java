package couk.jamietanna.disarm.elf;

public class ELFSymbolTableEntry {
	public enum ELFSymbolTableEntryType {
		FILE, FUNCTION, HI_PROC, LO_PROC, NO_TYPE, OBJECT, SECTION;

		@Override
		public String toString() {
			switch (this) {
			case FILE:
				return "File";
			case FUNCTION:
				return "Function";
			case HI_PROC:
				return "Processor-specific";
			case LO_PROC:
				return "Processor-specific";
			case NO_TYPE:
				return "No type";
			case OBJECT:
				return "Data object";
			case SECTION:
				return "Section";
			default:
				return "INVALID";
			}
		};
	}

	private short info;

	private long name;

	private short other;

	private int shndx;

	private long size;

	private long value;

	public int getBind() {
		return getInfo() & 0xf;
	}

	public short getInfo() {
		return info;
	}

	public long getName() {
		return name;
	}

	public short getOther() {
		return other;
	}

	public int getShndx() {
		return shndx;
	}

	public long getSize() {
		return size;
	}

	public ELFSymbolTableEntryType getType() {
		switch (getInfo() >> 4) {
		default:
		case 0:
			return ELFSymbolTableEntryType.NO_TYPE;
		case 1:
			return ELFSymbolTableEntryType.OBJECT;
		case 2:
			return ELFSymbolTableEntryType.FUNCTION;
		case 3:
			return ELFSymbolTableEntryType.SECTION;
		case 4:
			return ELFSymbolTableEntryType.FILE;
		case 13:
			return ELFSymbolTableEntryType.LO_PROC;
		case 15:
			return ELFSymbolTableEntryType.HI_PROC;
		}
	}

	public long getValue() {
		return value;
	}

	public void setInfo(short info) {
		this.info = info;
	}

	public void setName(long name) {
		this.name = name;
	}

	public void setOther(short other) {
		this.other = other;
	}

	public void setShndx(int shndx) {
		this.shndx = shndx;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public void setValue(long value) {
		this.value = value;
	}
}