package couk.jamietanna.disarm.elf;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import couk.jamietanna.disarm.elf.ELFSymbolTableEntry.ELFSymbolTableEntryType;
import couk.jamietanna.helper.FileMethods;

public class ELFSymbolTable {

	List<ELFSymbolTableEntry> entries = new ArrayList<ELFSymbolTableEntry>();

	public ELFSymbolTable(RandomAccessFile in, boolean isLittleEndian,
			ELFSectionHeaderEntry symbolTable, int tableCount)
			throws IOException {
		populate(in, isLittleEndian, symbolTable, tableCount);
	}

	public List<ELFSymbolTableEntry> getEntries() {
		return entries;
	}

	public void output() {

		System.out
				.println("Name\tMemory Address\tSize\tType\tBind\tOther\tShIdx");

		for (ELFSymbolTableEntry e : entries) {

			int index = e.getShndx();

			System.out.print(ELFFile.getInstance().getStringFrom(e.getName()));
			System.out.print("\t");

			ELFSymbolTableEntryType type = e.getType();

			if (index == -15)
				System.out.print("EQU #" + e.getValue());
			else if (type == ELFSymbolTableEntryType.NO_TYPE)
				System.out.print(String.format("0x%x", e.getValue()));
			else
				System.out.print(e.getValue());
			System.out.print("\t");

			System.out.print(e.getSize());
			System.out.print("\t");

			System.out.print(type);
			System.out.print("\t");

			System.out.print(e.getBind());
			System.out.print("\t");

			System.out.print(e.getOther());
			System.out.print("\t");

			switch (index) {
			// 0xFFF1
			case -15:
				System.out.print("Compiler Constant");
				break;
			case 0:
				System.out.println("Undefined");
				break;
			default:
				System.out.print("Program Header " + index);
				break;
			}
			System.out.print("\t");

			System.out.println();

		}
	}

	public void populate(RandomAccessFile in, boolean isLittleEndian,
			ELFSectionHeaderEntry symbolTable, int tableCount)
			throws IOException {

		in.seek(symbolTable.getOffset());

		for (int i = 0; i < tableCount; i++) {

			ELFSymbolTableEntry currentEntry = new ELFSymbolTableEntry();

			currentEntry.setName(FileMethods.readUInt(in, isLittleEndian));
			currentEntry.setValue(FileMethods.readUInt(in, isLittleEndian));
			currentEntry.setSize(FileMethods.readUInt(in, isLittleEndian));
			currentEntry.setInfo(FileMethods.readUByte(in));
			currentEntry.setOther(FileMethods.readUByte(in));
			currentEntry.setShndx(FileMethods.readUShort(in));

			entries.add(currentEntry);
		}

	}

	public void setEntries(List<ELFSymbolTableEntry> entries) {
		this.entries = entries;
	}

}
