package couk.jamietanna.disarm.elf;

import java.io.IOException;
import java.io.RandomAccessFile;

public class ELFFile {
	/**
	 * Getter
	 * 
	 * @return the programHeaderWrapper
	 */
	public ELFProgramHeaderWrapper getProgramHeaderWrapper() {
		return programHeaderWrapper;
	}

	/**
	 * Setter
	 * 
	 * @param programHeaderWrapper
	 *            the programHeaderWrapper to set
	 */
	public void setProgramHeaderWrapper(
			ELFProgramHeaderWrapper programHeaderWrapper) {
		this.programHeaderWrapper = programHeaderWrapper;
	}

	/**
	 * Getter
	 * 
	 * @return the sectionHeaderWrapper
	 */
	public ELFSectionHeaderWrapper getSectionHeaderWrapper() {
		return sectionHeaderWrapper;
	}

	/**
	 * Setter
	 * 
	 * @param sectionHeaderWrapper
	 *            the sectionHeaderWrapper to set
	 */
	public void setSectionHeaderWrapper(
			ELFSectionHeaderWrapper sectionHeaderWrapper) {
		this.sectionHeaderWrapper = sectionHeaderWrapper;
	}

	private static ELFFile instance;

	public static ELFFile decodeElfFile(String elfPath)
			throws InvalidELFFileException, IOException {

		ELFFile e = new ELFFile(elfPath);
		// TODO remove this
		instance = e;
		return e;
	}

	// TODO remove this, or hashmap the filename to an ELFFile?
	public static ELFFile getInstance() {
		return instance;
	}

	// private ElfDissasemblerAdapter adapter;
	private ELFHeader header;

	private RandomAccessFile in;

	private ELFProgramHeaderWrapper programHeaderWrapper;

	private ELFStringTable sectHdrStringTable;

	private ELFSectionHeaderWrapper sectionHeaderWrapper;

	private ELFStringTable stringTable;

	private ELFSymbolTable symbolTable;

	private ELFFile(String elfPath) throws InvalidELFFileException, IOException {
		in = new RandomAccessFile(elfPath, "r");
		header = new ELFHeader(in);
		if (header.isValidHeader() == false)
			throw new InvalidELFFileException();

		sectionHeaderWrapper = new ELFSectionHeaderWrapper(in, header);

		stringTable = new ELFStringTable(in, header.isLittleEndian(),
				sectionHeaderWrapper.getStringTable());

		sectHdrStringTable = new ELFStringTable(in, header.isLittleEndian(),
				sectionHeaderWrapper.getShstringTable());

		symbolTable = new ELFSymbolTable(in, header.isLittleEndian(),
				sectionHeaderWrapper.getSymbolTable(),
				stringTable.getTableCount());

		programHeaderWrapper = new ELFProgramHeaderWrapper(in, header);

		for (ELFProgramHeaderEntry e : programHeaderWrapper.getProgramHeaders())
			e.populateInstructions();
		// in.seek(e.getOffset());
		// // System.out.println(e);
		// // System.out.println(e.getFilesize());
		// // System.out.println("COUNT: " + e.getCount());
		// for (int i = 1; i <= e.getCount(); i++) {
		// System.out.print("Instruction " + i + "/" + e.getCount()
		// + " - ");
		// long m = FileMethods.readUInt(in, header.isLittleEndian());
		// String s = String.format("%08x", m);
		// System.out.print("0x" + s.substring(s.length() - 8));
		// System.out.println();
		// }

	}

	public ELFHeader getHeader() {
		return header;
	}

	public RandomAccessFile getInputFile() {
		return in;
	}

	public ELFSectionHeaderWrapper getSectionHeader() {
		return sectionHeaderWrapper;
	}

	public String getStringFrom(long name) {
		return stringTable.getStringFrom(name);
	}

	public String getStringFromSectHdr(long name) {
		return sectHdrStringTable.getStringFrom(name);
	}

	public ELFStringTable getStringTable() {
		return stringTable;
	}

	public ELFSymbolTable getSymbolTable() {
		return symbolTable;
	}

	public void output() {
		System.out.println("--Header--");
		header.output();

		System.out.println("--Sect Header--");
		sectionHeaderWrapper.output();

		System.out.println("--SymTab--");
		symbolTable.output();

		System.out.println("---StrTab---");
		stringTable.output();

		System.out.println("---ShStrTab---");
		sectHdrStringTable.output();

		System.out.println("---Program Headers---");
		programHeaderWrapper.output();

	}

	public void setHeader(ELFHeader header) {
		this.header = header;
	}

	public void setIn(RandomAccessFile in) {
		this.in = in;
	}

	public void setSectionHeader(ELFSectionHeaderWrapper sectionHeader) {
		sectionHeaderWrapper = sectionHeader;
	}

	public void setStringTable(ELFStringTable stringTable) {
		this.stringTable = stringTable;
	}

	public void setSymbolTable(ELFSymbolTable symbolTable) {
		this.symbolTable = symbolTable;
	}
}
