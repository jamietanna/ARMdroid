package couk.jamietanna.disarm.elf;

public enum ELFFileTypes {
	CORE_FILE, EXECUTABLE_FILE, NO_FILE_TYPE, RELOCATABLE_FILE, SHARED_OBJECT_FILE;

	@Override
	public String toString() {
		switch (this) {
		case CORE_FILE:
			return "Core File";
		case EXECUTABLE_FILE:
			return "Relocatable file";
		case NO_FILE_TYPE:
			return "Executable file";
		case RELOCATABLE_FILE:
			return "Shared object file";
		case SHARED_OBJECT_FILE:
			return "Core file";
		default:
			return "INVALID";
		}
	}

}
