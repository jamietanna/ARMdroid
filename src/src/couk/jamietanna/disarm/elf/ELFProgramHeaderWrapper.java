package couk.jamietanna.disarm.elf;

import java.io.IOException;
import java.io.RandomAccessFile;

import couk.jamietanna.helper.FileMethods;

public class ELFProgramHeaderWrapper {

	private ELFProgramHeaderEntry[] programHeaders;

	public ELFProgramHeaderWrapper(RandomAccessFile in, ELFHeader header)
			throws IOException {
		populate(in, header);
	}

	public ELFProgramHeaderEntry[] getProgramHeaders() {
		return programHeaders;
	}

	public void output() {
		System.out
				.println("Index\tType\tOffset\tFlags\tAlign\tVirt\tPhys\tSize\tMemsize");
		for (int i = 0; i < programHeaders.length; i++) {
			System.out.print(i + 1 + "\t");
			programHeaders[i].output();
		}
	}

	public void populate(RandomAccessFile in, ELFHeader header)
			throws IOException {

		programHeaders = new ELFProgramHeaderEntry[header.getpHdrCount()];

		in.seek(header.getPhdrOffset());

		for (int i = 0; i < header.getpHdrCount(); i++) {

			ELFProgramHeaderEntry currentEntry = new ELFProgramHeaderEntry(in,
					header.isLittleEndian());

			// putting filesize, memsize before virt/phys address - for some
			// reason doesn't read them the right way round
			currentEntry.setType(FileMethods.readUInt(in,
					header.isLittleEndian()));
			currentEntry.setOffset(FileMethods.readUInt(in,
					header.isLittleEndian()));
			currentEntry.setVirtaddr(FileMethods.readUInt(in,
					header.isLittleEndian()));
			currentEntry.setPhysaddr(FileMethods.readUInt(in,
					header.isLittleEndian()));
			currentEntry.setFilesize(FileMethods.readUInt(in,
					header.isLittleEndian()));
			currentEntry.setMemsize(FileMethods.readUInt(in,
					header.isLittleEndian()));
			currentEntry.setFlags(FileMethods.readUInt(in,
					header.isLittleEndian()));
			currentEntry.setAlign(FileMethods.readUInt(in,
					header.isLittleEndian()));

			programHeaders[i] = currentEntry;
			in.seek(header.getPhdrOffset()
					+ ((i + 1) * header.getpHdrEntSize()));
		}

	}

	public void setProgramHeaders(ELFProgramHeaderEntry[] programHeaders) {
		this.programHeaders = programHeaders;
	}
}
