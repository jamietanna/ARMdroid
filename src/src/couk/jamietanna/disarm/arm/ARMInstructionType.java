package couk.jamietanna.disarm.arm;

public enum ARMInstructionType {
	SWI, MULTIPLY, BRANCH, LOADSTORE, DATAPROCESSING, MULTIPLELOADSTORE, ADR;

	@Override
	public String toString() {
		switch (this) {
		case BRANCH:
			return "Branch";
		case DATAPROCESSING:
			return "Data Processing";
		case LOADSTORE:
			return "Load/Store";
		case MULTIPLELOADSTORE:
			return "Multiple Load/Store";
		case MULTIPLY:
			return "Multiply";
		case SWI:
			return "SWI";
		case ADR:
			return "Address Load";
		}
		return "--Invalid--";
	};
}