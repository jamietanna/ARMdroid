/**
 * 
 */
package couk.jamietanna.disarm.arm;

import couk.jamietanna.disarm.arm.ARMInstruction.ARMInstructionType;

/**
 * @author jvt02u (Jamie Tanna)
 * 
 */
public class ARMSWIInstruction implements IARMInstruction {

	private long instruction;

	private ARMOperand operand;

	private int memoryAddress;

	@Override
	public int getMemoryAddress() {
		return memoryAddress;
	}

	@Override
	public void setMemoryAddress(int memoryAddress) {
		this.memoryAddress = memoryAddress;
	}
	public ARMSWIInstruction(long instruction, int memoryAddress) {
		setInstruction(instruction);
		setOperand(instruction);
		setMemoryAddress(memoryAddress);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see couk.jamietanna.disarm.arm.IARMInstruction#decode()
	 */
	@Override
	public String decode() {
		return "SWI " + ((instruction & 0x00FFFFFF) >> 0);
	}

	/**
	 * Getter
	 * 
	 * @return the instruction
	 */
	@Override
	public long getInstruction() {
		return instruction;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see couk.jamietanna.disarm.arm.IARMInstruction#getOperand()
	 */
	@Override
	public ARMOperand getOperand() {
		// TODO Auto-generated method stub
		return operand;
	}

	/**
	 * Setter
	 * 
	 * @param instruction
	 *            the instruction to set
	 */
	@Override
	public void setInstruction(long instruction) {
		this.instruction = instruction;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * couk.jamietanna.disarm.arm.IARMInstruction#setOperand(couk.jamietanna
	 * .disarm.arm.ARMOperand)
	 */
	@Override
	public void setOperand(ARMOperand operand) {
		this.operand = operand;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see couk.jamietanna.disarm.arm.IARMInstruction#setOperand(long)
	 */
	@Override
	public void setOperand(long instruction) {
		operand = new ARMOperand(instruction);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see couk.jamietanna.disarm.arm.IARMInstruction#getInstructionType()
	 */
	@Override
	public ARMInstructionType getInstructionType() {
		return ARMInstructionType.SWI;
	}

}
