package couk.jamietanna.disarm.arm;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Adapted from code in
 * http://stackoverflow.com/questions/11550118/how-to-get-enums-numeric-value
 * 
 * @author jvt02u (Jamie Tanna)
 * 
 */
public enum ARMRegister {
	R1(1), R2(2), R3(3), R4(4), R5(5), R6(6), R7(7), R8(8), R9(9), R10(10), R11(
			11), R12(12), R13(13), R14(14), R15(15);

	public int getRegisterNumber() {
		return registerNumber;
	}

	private static final Map<Integer, ARMRegister> lookup = new HashMap<Integer, ARMRegister>();

	static {
		for (ARMRegister r : EnumSet.allOf(ARMRegister.class))
			lookup.put(r.getRegisterNumber(), r);
	}

	@Override
	public String toString() {
		switch (this) {
		case R11:
			return "FP";
		case R13:
			return "SP";
		case R14:
			return "LR";
		case R15:
			return "PC";
		default:
			return String.format("R%d", registerNumber);
		}
	}

	private int registerNumber;

	private ARMRegister(int registerNumber) {
		this.registerNumber = registerNumber;
	}

	public static ARMRegister get(int registerNumber) {
		return lookup.get(registerNumber);
	}

}
