package couk.jamietanna.disarm.arm;

public enum ARMDataProcessingInstructionType {
	ADC, ADD, AND, BIC, EOR, MOV, MVN, ORR, RSB, RSC, SBS, SMN, SMP, SUB, TEQ, TST
}
