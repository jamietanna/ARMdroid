package couk.jamietanna.disarm.arm;

public enum ARMShift {
	LSL, LSR, ASR, ROR, RRX
}