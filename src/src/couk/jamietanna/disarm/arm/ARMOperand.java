/**
 * 
 */
package couk.jamietanna.disarm.arm;

/**
 * @author jvt02u (Jamie Tanna)
 * 
 */
public class ARMOperand {
	private long theInstruction;

	public ARMOperand(long instruction) {
		theInstruction = instruction;
	}

	private int getRotateAmount() {
		return (int) ((theInstruction & 0x00000F00) >> 8);
	}

	private int getRotateImmediate() {
		return (int) (theInstruction & 0x00000FF);
	}

	private int getShiftImmediate() {
		return (int) ((theInstruction & 0x00000F80) >> 7);
	}

	private ARMRegister getShiftRm() {
		return ARMRegister.get((int) ((theInstruction & 0x0000000F) >> 0));
	}

	private ARMRegister getShiftRs() {
		return ARMRegister.get((int) ((theInstruction & 0x00000F00) >> 8));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return decode();
	}

	public String decode() {
		StringBuilder str = new StringBuilder();

		if (isRotate()) {
			if (getRotateAmount() == 0)
				str.append(String.format("#%d", getRotateImmediate()));
			else
				str.append(String.format("#%d", performRotate()));
		} else if (getShiftImmediate() == 0)
			str.append(String.format("%s", getShiftRm()));
		else {

			if (getShiftType() == ARMShift.RRX)
				str.append(", " + ARMShift.RRX + " ");

			if (((theInstruction & 0x00000010) >> 4) == 1)
				str.append(String.format("%s", getShiftRs()));
			else
				str.append(String.format("#%d",
						((theInstruction & 0x00000F80) >> 7)));
		}
		return str.toString();
	}

	public boolean isRotate() {
		return (theInstruction & 0x02000000) >> 25 == 1;
	}

	// TODO
	private int performRotate() {
		long rotAmount = getRotateAmount() * 2;
		long rotImmed = getRotateImmediate() >> 0;

		long mask = (1 << rotImmed) - 1;
		long lo = rotAmount & mask;
		long hi = rotAmount >> rotImmed;

		return (int) ((lo << (32 - rotImmed)) | hi);
	}

	public ARMShift getShiftType() {
		int shiftType = (int) ((theInstruction & 0x00000060) >> 5);
		switch (shiftType) {
		case 0:
			return ARMShift.LSL;
		case 1:
			return ARMShift.LSR;
		case 2:
			return ARMShift.ASR;
		case 3:
			return ARMShift.ROR;
		case 4:
			return ARMShift.RRX;
		}
		return null;
	}

};
