/**
 * 
 */
package couk.jamietanna.disarm.arm;

import couk.jamietanna.disarm.arm.ARMInstruction.ARMInstructionType;

/**
 * @author jvt02u (Jamie Tanna)
 * 
 */
public interface IARMInstruction {

	public abstract int getMemoryAddress();

	public abstract void setMemoryAddress(int memoryAddress);

	public abstract ARMOperand getOperand();

	public abstract void setOperand(ARMOperand operand);

	public abstract String decode();

	public abstract void setOperand(long instruction);

	public long getInstruction();

	public void setInstruction(long instruction);

	public ARMInstructionType getInstructionType();

}