package couk.jamietanna.disarm.arm;

public class ARMInstruction {

	private static ARMInstructionType getTypeFromInstruction(long instruction) {
		if (((instruction & 0x0F000000) >> 24) == 15)
			return ARMInstructionType.SWI;
		else if (((instruction & 0x0E000000) >> 25) == 5)
			return ARMInstructionType.BRANCH;
		else if (((instruction & 0x0E000000) >> 25) == 4)
			return ARMInstructionType.MULTIPLELOADSTORE;
		else if (((instruction & 0x0C000000) >> 26) == 1)
			return ARMInstructionType.LOADSTORE;
		else if ((((instruction & 0x0FC00000) >> 22) == 0)
				&& (((instruction & 0x00000090) >> 4) == 9))
			return ARMInstructionType.MULTIPLY;
		else if (((instruction & 0x0C000000) >> 26) == 0)
			return ARMInstructionType.DATAPROCESSING;
		else
			return null;
	}

	public static IARMInstruction decodeInstruction(long instruction, int memoryAddress) {
		ARMInstructionType type = getTypeFromInstruction(instruction);

		if (type == ARMInstructionType.SWI)
			return new ARMSWIInstruction(instruction, memoryAddress);
		else if (type == ARMInstructionType.BRANCH)
			return new ARMBranchInstruction(instruction, memoryAddress);
		else if (type == ARMInstructionType.MULTIPLELOADSTORE)
			return new ARMMultipleLoadStoreInstruction(instruction, memoryAddress);
		else if (type == ARMInstructionType.LOADSTORE)
			return new ARMLoadStoreInstruction(instruction, memoryAddress);
		else if (type == ARMInstructionType.MULTIPLY)
			return new ARMMultiplyInstruction(instruction, memoryAddress);
		else if (type == ARMInstructionType.DATAPROCESSING)
			return new ARMDataProcessingInstruction(instruction, memoryAddress);
		else
			return null;

	}

	public enum ARMInstructionType {
		SWI, MULTIPLY, BRANCH, LOADSTORE, DATAPROCESSING, MULTIPLELOADSTORE, ADR;

		@Override
		public String toString() {
			switch (this) {
			case BRANCH:
				return "Branch";
			case DATAPROCESSING:
				return "Data Processing";
			case LOADSTORE:
				return "Load/Store";
			case MULTIPLELOADSTORE:
				return "Multiple Load/Store";
			case MULTIPLY:
				return "Multiply";
			case SWI:
				return "SWI";
			case ADR:
				return "Address Load";
			}
			return "--Invalid--";
		};
	}

}
