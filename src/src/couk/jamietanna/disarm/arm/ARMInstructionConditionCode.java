package couk.jamietanna.disarm.arm;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Adapted from code in
 * http://stackoverflow.com/questions/11550118/how-to-get-enums-numeric-value
 * 
 * @author jvt02u (Jamie Tanna)
 * 
 */
public enum ARMInstructionConditionCode {
	EQ(0), NE(1), CS_HS(2), CC_LO(3), MI(4), PL(5), VS(6), VC(7), HI(8), LS(9), GE(
			10), LT(11), GT(12), LE(13), AL(14);

	/**
	 * Getter
	 * 
	 * @return the codeNum
	 */
	public int getCodeNum() {
		return codeNum;
	}

	public static ARMInstructionConditionCode getConditionCodeFromInstruction(
			long instruction) {
		return lookup.get((int) ((instruction & 0xF0000000) >> 28));
	}

	private static final Map<Integer, ARMInstructionConditionCode> lookup = new HashMap<Integer, ARMInstructionConditionCode>();

	static {
		for (ARMInstructionConditionCode r : EnumSet
				.allOf(ARMInstructionConditionCode.class))
			lookup.put(r.getCodeNum(), r);
	}

	private int codeNum;

	private ARMInstructionConditionCode(int codeNum) {
		this.codeNum = codeNum;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		// because we don't display AL
		if (this == AL)
			return "";
		else
			return super.toString();
	}
}
