package couk.jamietanna.helper;

import java.io.IOException;
import java.io.RandomAccessFile;

public class FileMethods {
	public static short readUByte(RandomAccessFile in) throws IOException {
		byte b = in.readByte();
		return (short) (b & 0xff);
	}

	public static long readUInt(RandomAccessFile in, boolean isLittleEndian)
			throws IOException {
		// http://stackoverflow.com/questions/9855087/converting-32-bit-unsigned-integer-big-endian-to-long-and-back
		int i = in.readInt();

		// code from
		// http://stackoverflow.com/questions/3842828/converting-little-endian-to-big-endian#comment15152643_5333066
		if (isLittleEndian) {
			int b0, b1, b2, b3;

			b0 = (i & 0xff) >> 0;
			b1 = (i & 0xff00) >> 8;
			b2 = (i & 0xff0000) >> 16;
			b3 = (i & 0xff000000) >>> 24;

			return ((b0 << 24) | (b1 << 16) | (b2 << 8) | (b3 << 0) & 0xffffffff);

		}

		return i;

	}

	public static int readUShort(RandomAccessFile in) throws IOException {
		short s = in.readShort();
		return (s >> 8 & 0xffffffff);
	}

}
