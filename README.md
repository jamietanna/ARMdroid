ARMdroid - *this is the droid you've been looking for*
======================================================

What is ARMdroid? 
-----------------
Have you ever had that overwhelming urge to code some ARM assembly while on the go? Well now you've got the chance! 

ARMdroid is an app that runs a virtual ARM processor to allow you to write your code and see it react faithfully to a real chip. 

Reasoning for the project 
-------------------------
My love of ARM assembly was sparked during my first term of university, where we were taught it as part of *Computer Systems Architecture*. Then as the final part of our *Introduction to Programming* module, we were tasked to build an [ARM ELF file (**E**xecutable and **L**inked **F**ormat) disassembler](https://github.com/jamietanna/disARM), which takes a compiled binary, and returns it to the original source code. 

However, I have much wanted to take this further, and I thought I would kill two birds with one stone and learn Android development while I'm at it. Short term, I will be writing a standard java application to mirror the functionality of disARM, and then I will integrate it into an Android application. 

Stages in ARMdroid
------------------
1. disARM - first I need to create disARM in Java, with good interfaces to allow all the data to seamlessly pass between objects.
1. ARMulator - next I will be building an ARM emulator that will faithfully respond to valid ARM instructions.
1. ARMdroid - finally I will put this all into an Android app, the structure and design of which is to be decided. 
